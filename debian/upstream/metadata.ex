# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/badwolf/issues
# Bug-Submit: https://github.com/<user>/badwolf/issues/new
# Changelog: https://github.com/<user>/badwolf/blob/master/CHANGES
# Documentation: https://github.com/<user>/badwolf/wiki
# Repository-Browse: https://github.com/<user>/badwolf
# Repository: https://github.com/<user>/badwolf.git
